﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sample.ViewModel
{
    public class BMIData
    {
        [Display(Name = "體重")]
        [Required(ErrorMessage ="required field")]
        [Range(30,150,ErrorMessage ="30~150")]
        public float Weight { get; set; }

        [Display(Name = "身高")]
        [Required(ErrorMessage = "required field")]
        [Range(50, 200, ErrorMessage = "50~200")]
        public float Height { get; set; }

        public float BMI { get; set; }

        public string Level { get; set; }
    }
}
